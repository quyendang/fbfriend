//
//  DataManager.swift
//  FBFriend
//
//  Created by Quyen Dang on 9/23/16.
//  Copyright © 2016 Quyen Dang. All rights reserved.
//

import UIKit

class DataManager: NSObject {
static let shareInstance = DataManager()
    var myFriendsList = [Friend]()
    
    override init() {
        print("Khoi tao")
    }
    func getUserFriendsWithId(userId : String! , callback : ([Friend]!) -> Void){
        var listFriends = [Friend]()
        
        let request : FBSDKGraphRequest = FBSDKGraphRequest.init(graphPath: userId, parameters: ["fields":"friends{id,name,picture.type(large)}"], HTTPMethod: "GET")
        request.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) in
            if error == nil {
                if let data = result as? [String : AnyObject]{
                    if let friends = data["friends"] as? [String : AnyObject], let ds = friends["data"] as? [[String : AnyObject]]{
                      for item in ds {
                        let fr = Friend.init(dic: item)
                        listFriends.append(fr)
                      }
                      
                      callback(listFriends)
                    }
                }
            }
        }
    }
}
