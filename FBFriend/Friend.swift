//
//  Friend.swift
//  FBFriend
//
//  Created by Quyen Dang on 9/23/16.
//  Copyright © 2016 Quyen Dang. All rights reserved.
//

import Foundation
class Friend : NSObject{
    var userId : String?
    var userName : String?
    var userAvata : String?
    
    init(dic : [String : AnyObject]!) {
        if let userId = dic["id"] as? String, let userName = dic["name"] as? String {
            self.userId = userId
            self.userName = userName
        }
        
        if let picture = dic["picture"] as? [String : AnyObject] {
            if let data = picture["data"] as? [String : AnyObject] {
                if let url = data["url"] as? String {
                    self.userAvata = url
                }
            }
        }
        
    }
}