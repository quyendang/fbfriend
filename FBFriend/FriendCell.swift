//
//  FriendCell.swift
//  FBFriend
//
//  Created by Quyen Dang on 9/23/16.
//  Copyright © 2016 Quyen Dang. All rights reserved.
//

class FriendCell: UITableViewCell {
    @IBOutlet weak var avataImageView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.avataImageView.layer.cornerRadius = self.avataImageView.frame.width / 2
        self.avataImageView.clipsToBounds = true
    }
    
}
