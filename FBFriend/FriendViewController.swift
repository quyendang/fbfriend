//
//  FriendViewController.swift
//  FBFriend
//
//  Created by Quyen Dang on 9/23/16.
//  Copyright © 2016 Quyen Dang. All rights reserved.
//

import UIKit
import SDWebImage
import FBSDKLoginKit

class FriendViewController: UIViewController, UITableViewDataSource, UITableViewDelegate , FBSDKLoginButtonDelegate{
    var refreshControl: UIRefreshControl!
    var frTable: UITableView!
    var userId : String!
    var lists : [Friend] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFaceBookAvatar()
        initTableView()
        initRefreshControl()
        DataManager.shareInstance.getUserFriendsWithId(self.userId) { (fr) in
            self.lists.appendContentsOf(fr)
            self.frTable.reloadData()
        }
    }
    func initTableView() {
      self.frTable = UITableView(frame: self.view.bounds)
      self.frTable.delegate = self
      self.frTable.dataSource = self
      self.frTable.registerNib(UINib(nibName: "FriendCell", bundle: nil), forCellReuseIdentifier: "friend")
      self.view.addSubview(self.frTable)
    }
    
    func initRefreshControl() {
      self.refreshControl = UIRefreshControl(frame: CGRect(x: 0, y: -50, width: self.frTable.frame.width, height: 50))
      self.refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
      self.frTable.addSubview(self.refreshControl)
    }
  
    func initFaceBookAvatar() {
        self.navigationItem.titleView = FBSDKProfilePictureView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.navigationItem.titleView?.layer.cornerRadius = 20
        self.navigationItem.titleView?.clipsToBounds = true
        let logoutButton = FBSDKLoginButton()
        logoutButton.frame.size = CGSize(width: 70, height: 20)
        logoutButton.delegate = self
        let logoutBarButton = UIBarButtonItem(customView: logoutButton)
        self.navigationItem.rightBarButtonItem = logoutBarButton
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("nav") as! UINavigationController
        let loginView = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("login") as! ViewController
        nav.setViewControllers([loginView], animated: true)
        UIApplication.sharedApplication().keyWindow?.rootViewController = nav
        self.navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
    }
    
    func refresh() {
        DataManager.shareInstance.getUserFriendsWithId(self.userId) { (fr) in
            self.lists.removeAll()
            self.lists.appendContentsOf(fr)
            self.frTable.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lists.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let fr = self.lists[indexPath.row]
        let cell = self.frTable.dequeueReusableCellWithIdentifier("friend") as! FriendCell
        cell.nameLabel?.text = fr.userName
        cell.avataImageView?.sd_setImageWithURL(NSURL(string: fr.userAvata!))
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let fr = self.lists[indexPath.row]
//        let myFrView = self.storyboard?.instantiateViewControllerWithIdentifier("myfriend") as! MyFriendViewController
//        myFrView.user = fr
//        self.navigationController?.pushViewController(myFrView, animated: true)
        let view = YourFriendViewController()
        view.userId = fr.userId
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.lists.count - 1 {
            print("load more \(indexPath.row)")
        }
    }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 70
  }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
