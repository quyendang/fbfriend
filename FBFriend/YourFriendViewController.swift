//
//  YourFriendViewController.swift
//  FBFriend
//
//  Created by Q on 9/24/16.
//  Copyright © 2016 Quyen Dang. All rights reserved.
//

import UIKit

class YourFriendViewController: FriendViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func initFaceBookAvatar() {
      let request : FBSDKGraphRequest = FBSDKGraphRequest.init(graphPath: userId, parameters: ["fields":"id,name,picture.type(large)"], HTTPMethod: "GET")
      request.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) in
        if error == nil {
          if let data = result as? [String : AnyObject]{
            if let friends = data["picture"] as? [String : AnyObject], let ds = friends["data"] as? [String : AnyObject], let url = ds["url"] as? String{
              let avata = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
              avata.sd_setImageWithURL(NSURL(string: url))
              avata.contentMode = .ScaleAspectFit
              avata.layer.cornerRadius = 20
              avata.clipsToBounds = true
              
              self.navigationItem.titleView = avata
              self.view.setNeedsDisplay()
//              self.navigationItem.titleView?.layer.cornerRadius = 20
//              self.navigationItem.titleView?.clipsToBounds = true
            }
          }
        }
      }
    }
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    override func viewWillAppear(animated: Bool) {
      self.navigationItem.hidesBackButton = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
